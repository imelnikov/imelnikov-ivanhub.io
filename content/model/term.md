title: Термины
layout: model/single.html

tag:
- радиоуправление
- самолет
- подводная лодка

---
Абревиатуры и англицизмы из различных областей моделизма.

## Радиоуправление
- RC - remote controll
- PWM (pulse width modulation) - протокол передачи "один канал на провод"
- PPM (pulse position modulation) - протокол передачи "все каналы по одному проводу"
- PCM (pulse code modulation) - тоже что PPM, но цифровой с защитой от помех
- SBUS serial bus - цифровой протокол от Futaba и FrSky
- DSM2, DSMX - цифровые протоколы от Spektrum

## Электроника
- ESC (electronic speed controll) - регулятор хода
- BEC (batery eliminate circit) - преобразователь питания
- brushed/brishless motor - коллекторный/бесколлекторный мотор
- dc motor - мотор постоянного тока

## Самолеты
- AIL (aileron) - элетон
- ELE (elevator) - руль высоты
- RUD (ruder) - руль направления
- THR (throtle) - газ
- pitch - тангаж, килевая качка
- roll - крен
- yaw - рысканье

## Подводные лодки
- WTC (water tight cylinder) - водонепроницаемый цилинд, прочный корпус
- ballast tank - балластная цистерна
- hull - корпус
- deck - палуба
- bow - нос
- stern - корма
- bulkhead - шпангоут, перегородка
- shaft - вал винта

# FPV
- FPV (First Point of View) - вид от первого лица
- OSD (On Screen Display) - наложение текста поверх видео

[Присылайте дополнения!](/contact)
