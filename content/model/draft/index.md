title: Чертежи
layout: model/single.html
meta: Чертежи радиоуправляемых моделей самолетов

---

## Самолеты

* [Миг-3 1:12 по бутылочной технологиии](/model/draft/mig-3)
* [Me-109e 1:12](/model/draft/me-109)
* [Cessna из потолочки](/model/draft/cessna)

## Лодки

* [Простая подводная лодка](/model/draft/submarine)
