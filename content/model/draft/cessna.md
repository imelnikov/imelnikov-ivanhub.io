title: Чертеж Cessna из потолочки
layout: model/single.html
disqus: True
meta: цессна из потолочки

tag:
 - чертеж
 - тренер
 - потолочка

---


Спецификация |
--------|--------
Размах  | 100 см
Длина   | 82 см
Вес     | 350 гр


Процесс постройки [Цессны из потолочки](/model/cessna).

Файлы
-----
 * [Cessna DXF](/file/cessna/cessna.dxf)
 * [киль pdf](/file/cessna/cessna1.pdf)
 * [стабилизатор pdf](/file/cessna/cessna2.pdf)
 