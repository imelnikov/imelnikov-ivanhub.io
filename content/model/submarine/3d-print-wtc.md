layout: model/single.html
title: 3d-печать прочного корпуса подводной лодки
date: 2017-07-24
cover: /img/submarine/wtc/wtc.png
disqus: True

tag:
 - подводная лодка
 - WTC
 - материалы
 - 3d-печать
 - чертеж

gallery:
 - /img/submarine/wtc/DSCN8691.JPG
 - /img/submarine/wtc/DSCN8693.JPG
 - /img/submarine/wtc/DSCN8692.JPG
 - /img/submarine/wtc/DSCN8694.JPG 

---

{% import 'macros.html' as macros %}

Заранее прошу прощения за некоторый сумбур в начале видео, дальше вроде лучше.

{{ macros.youtube("85WkGM5BzDQ") }}

Печатаем на 3д-принтере детали прочного корпуса модели подводной лодки. В данном видео рассматриваются вопросы выбора:
- параметры печати принтера
- способы повышения адгезии
- программы слайсеры (Cura vs Slicr3d)
- плотность заполнения

Общее время печати всех деталей - более 6 часов.

{{ macros.fotorama_init() }}
{{ macros.fotorama(page.gallery) }}

STL-файлы можно посмотреть и скачать на [github'e](https://github.com/melnikov-ivan/WTC) или на странице в [Thingiverse](https://www.thingiverse.com/thing:2468467)

- передняя и задняя заглушки с патрубками тяг рулей управления
- детали 3го отсека (площадки под сервы и регулятор хода)

Как создать [3d-модель подводной лодки](/model/3d)?
