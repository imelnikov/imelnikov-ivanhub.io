title: Объем балластной цистерны
layout: model/single.html
date: 2016-10-01
cover: /img/submarine/ballast.png
disqus: True
meta: Расчет балластной цистерны модели подводной лодки

tag:
 - подводная лодка
 - теория

---

Внутреннее устройство [радиоуправляемой подводной лодки](/model/submarine) похоже на настоящую лодку. Она состоит из 3 основных частей:
* легкий корпус 
* прочный корпус (WTC - water tight cylinder)
* балластная цистерна - БЦ

Рекомендую изучить прочие [термины судомоделизма](/model/term).

![основные компоненты подводной лодки](/img/submarine/sub-plan.png)

Давайте разберемся как же подводная лодка погружается. Это поможет нам выбрать правильные компоненты при проектировании. Итак.

Теория погружения
-----------------

В погруженном состоянии легкий корпус заполнен водой, внутри него распологается WTC (прочный корпус) со всей электроникой. В том числе там же распологается и балластная цистерна (БЦ), которая качает забортную воду внутрь прочного корпуса, тем самым увеличивая его массу.

Для расчета размеров БЦ нужно вспомнить школьную физику и закон Архимеда. Пусть, 
* V1 - объем надводной части
* V2 - объем подводной части
* Vb - объем БЦ (входит в объем V2)
* М - масса всей лодки
* R - плотность воды

![закон архимеда для подводной лодки](/img/submarine/arhimed.png)

Закон Архимеда гласит, что выталкивающая сила равна массе вытесненной воды. Если лодка на поверхности, получаем.

`R * V2 = M`

В погруженном состоянии сила Архимеда, должна быть меньше либо равна массе лодки и воды в БЦ.

`R (V1 + V2) < M + (R * Vb)`

Подставляем М из первого уравнения

```
RV1 + RV2 < RV2 + RVb
RV1 < RVb
V1 < Vb
```

Самый главный вывод этой статьи: 

**Объем надводной части должен быть меньше объема БЦ.**


Выводы
------

Самая частая ошибка - неправильно понимают слова "надоводной части". Имеется в виду не объем всей лодки, выступающий надо водой (как если бы мы перевернули лодку, и налили воду внутрь легкого корпуса по ватерлинию). А только объем стенок легкого корпуса, т.к. воздух из под него выйдет через технические отверстия во время погружения.

**WTC желательно распологать ниже ватерлинии**. Потому что весь объем WTC выше ватерлинии подпадает под определение "надводной части". Воздух из него не выйдет. А это потребует значительно увеличить объем БЦ (балластной цистерны).

Почему нельзя сделать большую балластную цистерну? Потому что она будет занимать значительную часть WTC. Потребуется более мощный двигатель, чтобы быстро заполнять воду (дольше 30 сек будет скучно). Мощный двигатель будет потреблять много тока, и время плавания сократится.

Лучше использовать БЦ только для поднятия из воды *полезной* нагрузки: рубка, высокие борта, палубные орудия, антенны и т.п.


Пример расчета
--------------

Давайте прикинем конфигурацию типичной модели подводной лодки. Как и во многих видах моделизма (да и нашей жизни), тут есть оптимальные размеры - от 1.0 до 1.5 метров. Лодка длиной 50см потребует редкие микродвигатели, микро-серв, тонкие валы (тоже редки) и т.п. Большую лодку длиной 2м будет уже неудобно транспортировать, она будет тяжелой, требовать мощные дорогие двигатели, дорогие аккумуляторы и т.п.

###Размеры
Для первой лодки подойдет размер 1м, давайте от него отталкиваться. Выберите понравившийся прототип. Обычно ширина лодки равна 1/10 ее длины, скажем 10см. Тогда приблизительная площадь легкого корпуса выступающая из воды будет 1000см². Если толщину стенок корпуса принять за 0.1см, то получаем объем выступающих частей 100мл. Значит БЦ должна быть не меньше 100мл.

###Объем БЦ
Для такой БЦ подойдет большой шприц на 150 мл (шприц Жане) и [перистальтический насос](https://goo.gl/mpOcAj).

###Диаметр WTC
Пусть высота нашей лодки 12см (без рубки), а ватерлиния 9см от киля. Если расположить под WTC 1 см свинцового балласта, то до ватерлинии остается место под трубу диаметром 70-80мм. Это нормальный размер, вы легко найдете моторы, сервы приемники, которые свободно расположатся внутри. Расположить всю электронику в трубе диаметром 50мм и меньше будет очень сложно.

###Длина WTC
Если в качестве БЦ использовать щприц на 150мл и насос, то вместе с аккумулятором, примеником, сервами и мотором то выйдет 50-60 см. Как раз остается по 20см спереди и сзади лодки на сужение сечения профиля лодки.

###Вес лодки
Объем такого WTC получается примерно 3 литра. Чтобы его утопить, масса лодки вместе с водой в БЦ должна быть больше 3кг. Т.к. БЦ у нас всего 150мл, то масса сухой долки получается около 3кг. Скорее всего потребуется свинцовый груз.

Читайте также
-------------

[3d-печать подводной лодки](/model/submarine/3d-print-wtc)

