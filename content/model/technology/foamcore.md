title: Крыло из Пенокартона
layout: model/single.html
date: 2015-05-01
cover: /img/mig/DSCN5719.JPG

tag:
 - пенокартон
 - крыло
---

Изготовление крыла из пенокартона не требует специальных оснасток, стапелей. 
В отличии от резки крыла нихромом, не выделяет вредные вещества. 
Все что вам нужно - это чертеж, нож и клей. 
Этап подготовки занимает чуть больше времени, т.к. нужно заранее расчитать развертку крыла. 
Не все можно потом исправить по месту. 

Итак рассмотрим процесс постройки крыла из пенокартона на примере [модели Миг-3](/model/mig-3). 
Размах - 85 см. Хорда в корне 22 см, на концах - 9 см. 
Т.к. предполагается использование модели в воздушном бою, то жесткость должна быть достаточной. 
Используем лонжерок состоящий из 2-х слоев пенокартона соединенных в районе центроплана внахлест.

Профиль крыла выберем плоско-выпуклый. 
При изготовлении чертежа следует заложить 0.5 см больше от плана с верху на изгиб поверхности. 
Рулевые машинки располагаются лежа в торцах крыла. 
Лонжерон и нижняя часть крыла покрывают 70% размаха. Элероны не на всю длину, близкие к копийным.


* детали

Выводы
------

При размахе 85 см вес получается приблизительно 120 гр без сервомашинок, что конечно многова-то. 
Для сравнения вырезанное нихромом из пенопласта весит 70 гр. 
Достаточно высокая жесткость и прочность. Но листовой материал не очень ремонто пригоден. 

Плюсом можно отметить чистоту технологии, нет стружки пенопласта и запаха гари. 
Поверхность готова под покраску.
