title: Программа для чертежей самолетов
layout: model/single.html
date: 2015-10-27
cover: /img/drafts/drafts-500.png
disqus: True
meta: Чертеж модели на компьютере

tag:
 - чертеж
 - программа
 - полукопия

---
{% import "macros.html" as macros %}

{{ macros.img(page.cover, (500, 500)) }}

При постройке даже маленькой модели чертеж просто незаменим. Обычно он хранятся в виде нарезки шаблонов (так называемой лапши), по которым потом вырезается деталь. При этом оригинал хранится в виде картинки. У этого подхода есть несколько больших минусов. Картинки тяжело приводить к нужному масштабу при печати. Их нельзя редактировать, если хочется что-то поменять. А распечатанные в нужном масштабе чертежи нельзя перекинуть другу по электронной почте. У всех этих проблем есть решение: чертеж модели на компьютере. 

Я сам долго не подходил к программам проектирования, боялся их сложности, думал, что усилия не стоят результата на маленьких моделях. Но всё это не так. Я раскажу вам, как сделать чертеж модели с фотографии оригинала, выполнить сразу в заданном масштабе и распечатать в нужном размере.


Как сделать чертеж модели на компьютере
---------------------------------------

Для примера пропробуем создать модель полукопию. За основу возьмем картинку в высоком разрешении с общим планом самолета и сечениями. Затем переведем ее в электронный вид. Отмасштабируем до нужных расмеров, это поможет понимать реальное расположение внутренних элементов. Добавим к чертежу двигатель и мотораму. И в конце выгрузим в pdf-файл в натуральную величину.


### Какая программа лучше для моделирования

Нам понадобится программа для 2D моделирования. Из функционала необходимы слои (они позволяют скрывать части чертежа и разграничивать уровни детализации) и рисование прямых/окружностей (собственно для создания модели).

Безусловно, платные версии CAD-приложений (САПР) обладают большим количеством функций. Но для наших целей хватит возможностей практически любой бесплатной программы. А вот возможности экспорта и печати могут различаться весьма значительно. Из бесплатных в итоге приемлемый результат удалось получить с помощью программ [QCAD](http://www.ribbonsoft.com/en) и [SketchUp](http://www.sketchup.com). Они доступны для всех операционных систем: Windows, Unix, MacOS.

### QCAD

Имеет бесплатную версию, платная стоит 30$. Поддерживает распространенные форматы DXF и DWG. Интерфейс на русском языке. Функция печати на некскольких листах реализована в версии PRO, которая доступна в бесплатной версии 20 минут, затем требует перезагрузки.


### SketchUp

Имеет бесплатную версию. Поддержка форматов DXF только в платной версии (500$), собственный формат ```.skp```. Поддерживает многостраничную печать. Существует центральная платформа для хранения моделей, куда можно выгружать свои модели, загружать чужие. Это очень здорово! Также SketchUp поддеживает возможность 3-х мерного проектирования.


Дальше будем рассмотривать работу в программе QCAD, т.к. она поддерживает распространенный формат DXF, в то время как формат SKP пока еще не очень стабилен.


Оригинальный чертеж, масштаб
----------------------------

загружаем картинку, приводим к заданному размеру. Создаем электронную версию чертежа

Редактирование
--------------

Создаем доп слои, редактируем.


Печать чертежа в масштабе
-------------------------



Как распечатать чертеж в натуральную величину?

[Чертежи моделей](/model/draft) можно найти в отдельном разделе.
