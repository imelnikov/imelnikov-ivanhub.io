title: Телеметрия
layout: radio/single.html

tag:
 - arduino
 - электроника
 - радиоуправление

---

*Страница в разработке*

*Посмотрите другие статьи в разделе [Электроника](/radio)*


Напряжение
==========
```c
int analogInput = A0;
float vout = 0.0;
float vin = 0.0;
float R1 = 30000.0; //  
float R2 = 7500.0; // 
int value = 0;
void setup(){
   pinMode(analogInput, INPUT);
   Serial.begin(9600);
   Serial.print("DC VOLTMETER");
}
void loop(){
   // read the value at analog input
   value = analogRead(analogInput);
   vout = (value * 5.0) / 1024.0; // see text
   vin = vout / (R2/(R1+R2)); 
   
Serial.print("INPUT V= ");
Serial.println(vin,2);
delay(500);
}
```

Глубина
=======
```c
// http://playground.arduino.cc/Code/NewPing

#include <NewPing.h>
 
#define TRIGGER_PIN  12
#define ECHO_PIN     11
#define MAX_DISTANCE 200
 
NewPing sonar(TRIGGER_PIN, ECHO_PIN, MAX_DISTANCE);
 
void setup() {
  Serial.begin(115200);
}
 
void loop() {
  delay(50);
  Serial.print("Ping: ");
  Serial.print(sonar.ping_cm());
  Serial.println("cm");
}

```

Примеры кода
------------

* [Подводный аппарат](https://edwardmallon.wordpress.com/)
* [MaxBotix](http://envirodiy.org/ultrasonic-water-depth-sensor/)
* [Библиотека](https://github.com/jeroendoggen/Arduino-distance-sensor-library/blob/master/DistanceSensor/DistanceSRF04.cpp)
* [pulseIn](http://www.tigoe.com/pcomp/code/arduinowiring/17/)
* [Теория](https://www.robot-electronics.co.uk/htm/srf05tech.htm)
* [Просто](http://arduinobasics.blogspot.ru/2012/11/arduinobasics-hc-sr04-ultrasonic-sensor.html)

