layout: dev/single.html
title: Git или Mercurial
#date: 2015-12-04
cover: /img/mouse/cover.png
disqus: True

tag:
 - git
 - hg
 - tools

---


Статья представляет из себя скорее справочник различий двух популярных систем контроля версий.


Branching
---------

 # Во время push git отправляет только текущую ветку, в то время как hg выгружает все ветки.
 # hg branch сохраняет имя ветки прямо в changeset'е, а hg bookmark 
 # master vs default


Rebase
------

Еще одним различием является реализация функции rebase.
