title: Хостинг статического сайта
layout: dev/single.html
date: 2016-06-13
cover: /img/codeship/deploy.png
disqus: True
meta: генератор статических сайтов, бесплатный хостинг

gallery:
 - /img/codeship/install.png
 - /img/codeship/build.png
 - /img/codeship/deploy.png

tag: 
- git
- hg
- python
- tools
---

{% import "macros.html" as macros %}
{{ macros.fotorama_init() }}

Статические сайты пользуются большой популярностью, т.к. обладают определенными преимуществами по сравнению с CMS
- простой хостинг
- любая структура сайта
- держит высокую нагрузку

Чтобы публикация статического сайта не вызывала страдания, надо как можно лучше автоматизировать этот процесс. Рассмотрим связку: Bitbucket → Tagy → Codeship → Github.

Генерация статического сайта
----------------------------

Я использую [Tagy](https://goo.gl/tr2yUc){# http://github.com/melnikov-ivan/tagy #}.  Он очень гибкий, а также обладает удобными встроенными плагинами для картинок и т.п. Для сборки требуется одна команда 
```
./tagy.py -b
```

, или из python
```
#!/usr/bin/env python
import tagy
tagy.generate()
```

Исходный код сайта (страницы, картинки, файлы) хранится в системе контроля версий [Bitbucket](https://bitbucket.org/imelnikov/imelnikov.ru/src). 

Codeship static site
--------------------

Текущий статус сборки [ ![Codeship Status for imelnikov/imelnikov.ru](https://app.codeship.com/projects/e7f7b5d0-09f9-0134-0986-0ed96050b2d1/status?branch=default)](https://app.codeship.com/projects/155411)

Для публикации используется сервис CI Codeship. Он подключается к репозиторию в BibBucket или GitHub и после каждого коммита выполняет предписанные действия:
 - выкачивает исходники (всегда, дефолтное поведение)
 - устанавливает генератор сайта
 - запускает сборку сайта
 - публикует готовый контент на хостинг (GH-pages)

Codeship имеет бесплатные варианты аккаунтов. Регистрируем свой репозиторий https://codeship.com/projects/new и переходим в настройки.

### Установка покетов
Setup command - скрипты для установки зависимостей
```
pip install tagy
```

### Генерация сайта
Configure test pipline - скрипты самой сборки. 
- Исходники по умолчанию загружаются в папку ```~/clone/```.
- в файле [build.py](https://bitbucket.org/imelnikov/imelnikov.ru/src/5fdc89b47b99bde5362dc611b8953e611f2cbfb5/build.py) команда для генерации
```
cd ~/clone/
python build.py
```

### Публикация 
На вкладке Deployment пишем что делать в случае успешной сборки. Я использую GitHub-Pages для хостинга, т.к. он проще всего настраивается и поддерживает доменные имена. Соответственно в скрипте пушим папку со сгенеренным сайтом в github.
На вкладке 'General' есть поле 'SSH public key'. Добавляем этот ключ в настройках github-репозитория (Settings -> Deploy keys)
```
cd public/

git config --global user.email "<your email>"
git config --global user.name "<your name>"

git add .
git commit -m "$CI_MESSAGE"
git push --force git@github.com:melnikov-ivan/melnikov-ivan.github.io.git

```

{{ macros.fotorama(page.gallery, width="800px", ratio="") }}

Теперь сайт доступен по адресу [melnikov-ivan.github.io](http://melnikov-ivan.github.io).

Custom domain for GitHub pages
------------------------------
Для подключения доменного имени к github-pages надо:
- добавить файл ```CNAME```, содержащий имя домена, в корень репозитория
- у регистратора доменов добавляем a-записи с ip-адресами github'а
```
192.30.252.153
192.30.252.154
```
Подробнее на [странице помощи github-pages](https://help.github.com/articles/setting-up-an-apex-domain/#configuring-a-records-with-your-dns-provider)

