Static site
===========

![codeship](https://codeship.com/projects/e7f7b5d0-09f9-0134-0986-0ed96050b2d1/status?branch=default)

Develop
-------

```
# виртуальное окружение
virtualenv venv

# активируем
source venv/bin/activate

# библиотека для картинок
sudo apt-get istall libjpeg-dev

# зависимости
sudo pip install tagy
```


Deploy
------

```
./tagy.py -b
rm -rf ../melnikov-ivan.github.io/*; cp -r public/* ../melnikov-ivan.github.io/
```

Codeship
--------
Настроена CI, детали в посте https://imelnikov.ru/dev/static-site/. Не переносит русские коммит-сообщения.

Plans
-----

https://trello.com/b/fah2Qyx8/imelnikov-ru


Params
------

### Built-in

 * name
 * url

### Custom
 * title 
 * layout